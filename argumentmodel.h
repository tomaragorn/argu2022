#ifndef ARGUMENTMODEL_H
#define ARGUMENTMODEL_H

#include <QObject>
#include<QSqlTableModel>

class ArgumentModel : public QSqlTableModel
{
    Q_OBJECT
    Q_PROPERTY(QString numeroFiche READ numeroFiche WRITE setNumeroFiche NOTIFY ficheChanged)
public:
    ArgumentModel(QObject *parent = nullptr);
    QString numeroFiche() const;
    //Q_INVOKABLE void setNumeroFiche(const QString &recipient);

    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
    static ArgumentModel* leModele;
signals:
    void ficheChanged();
public slots:
    Q_INVOKABLE void setNumeroFiche(const QString &recipient);
private:
    QString m_numeroFiche="1";
};
#endif // ARGUMENTMODEL_H
