#include "argumentmodel.h"
#include <QSqlQuery>
#include <QSqlRecord>
#include <QDebug>

ArgumentModel* ArgumentModel::leModele=nullptr;
/**
 * @brief ArgumentModel::ArgumentModel
 * @param parent
 * Constructeur d'AgumentModel, obtient les données de la première fiche
 */
ArgumentModel::ArgumentModel(QObject *parent) :
    QSqlTableModel(parent)
{
    qDebug()<<"ArgumentModel::ArgumentModel(QObject *parent)";
    setTable("Argument");
    const QString filterString="numeroMesure=1";
    setFilter(filterString);
    select();
    ArgumentModel::leModele=this;
}
/**
 * @brief ArgumentModel::numeroFiche
 * @return le numéro de fiche courant
 */
QString ArgumentModel::numeroFiche() const
{
    return this->m_numeroFiche;
}
/**
 * @brief ArgumentModel::setNumeroFiche
 * @param numeroFiche
 * Change le filtre opéré sur la table pour obtenir les données de la fiche
 */
void ArgumentModel::setNumeroFiche(const QString &numeroFiche)
{
    qDebug()<<"void ArgumentModel::setNumeroFiche(const QString &numeroFiche)";
    qDebug()<<"Numéro fiche:"<<numeroFiche;
    if(numeroFiche==m_numeroFiche)
        return;
    this->m_numeroFiche=numeroFiche;
    const QString filterString="numeroMesure="+numeroFiche+"";
    setFilter(filterString);
    select();
    emit ficheChanged();
}
/**
 * @brief ArgumentModel::data
 * @param index
 * @param role
 * @return la valeur du modèle associé à l'index et au rôle fournis en paramètres
 */
QVariant ArgumentModel::data(const QModelIndex &index, int role) const
{
    qDebug()<<"QVariant ArgumentModel::data(const QModelIndex &index, int role) const";
    if (role < Qt::UserRole)
        return QSqlTableModel::data(index, role);

    const QSqlRecord sqlRecord = record(index.row());
    return sqlRecord.value(role - Qt::UserRole);
}
/**
 * @brief ArgumentModel::roleNames
 * @return une hash numeroDeRôle->nomRole
 */
QHash<int, QByteArray> ArgumentModel::roleNames() const
{
    QHash<int, QByteArray> names;
       names[Qt::UserRole] = "numeroArgument";
       names[Qt::UserRole + 1] = "titreArgument";
       names[Qt::UserRole + 2] = "texteArgument";
       names[Qt::UserRole + 3] = "flecheArgument";
       names[Qt::UserRole + 4] = "iconeArgument";
       names[Qt::UserRole + 5] = "numeroMesure";
       return names;
}
