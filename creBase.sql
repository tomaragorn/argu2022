CREATE TABLE `Fiche`(`numeroFiche` INTEGER,`titreSupFiche` VARCHAR(100),`titreInfFiche` VARCHAR(100),`couleurFiche` VARCHAR(8),primary key(`numeroFiche`));

CREATE TABLE `ChiffreCle`(`numeroChiffreCle` INTEGER,`chiffre` INTEGER,`unite` VARCHAR(12),`texteChiffre` VARCHAR(150),`iconeChiffre` VARCHAR(150),`numeroFiche` INTEGER NOT NULL, foreign key (`numeroFiche`) references Fiche(`numeroFiche`),primary key(`numeroFiche`,numeroChiffreCle));

CREATE TABLE `Mesure`(`numeroMesure` INTEGER,`libelleMesures` VARCHAR(254),`numeroFiche` INTEGER NOT NULL, foreign key (`numeroFiche`) references Fiche(`numeroFiche`),primary key(`numeroMesure`));

CREATE TABLE `Argument`(`numeroArgument` INTEGER,`titreArgument` VARCHAR(100),`texteArgument` VARCHAR(250),`flecheArgument` VARCHAR(200),`iconeArgument` VARCHAR(250),`numeroMesure` INTEGER NOT NULL, foreign key (`numeroMesure`) references Mesure(`numeroMesure`),primary key(`numeroMesure`,numeroArgument));

CREATE TABLE `DetailMesure`(`numeroDetailMesure` INTEGER,`texteDetailMesure` VARCHAR(300),`numeroMesure` INTEGER NOT NULL, foreign key (`numeroMesure`) references Mesure(`numeroMesure`),primary key(`numeroMesure`,numeroDetailMesure));

