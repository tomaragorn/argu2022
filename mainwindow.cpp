#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include "argumentmodel.h"
#include <QScreen>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    numeroFicheCourante=1;
    obtenirFiche();
    statusBar()->hide();
    QSize size = qApp->primaryScreen()->availableSize();
    this->setFixedSize(size);
}

MainWindow::~MainWindow()
{
    delete ui;
}
/**
 * @brief MainWindow::obtenirFiche
 * Obtention de la première fiche depuis la base de données
 */
void MainWindow::obtenirFiche()
{
    qDebug()<<"void MainWindow::obtenirFiche()";
    QString txtReqFiche="select numeroFiche,titreSupFiche,titreInfFiche,couleurFiche from Fiche";
    reqFiche.exec(txtReqFiche);
    reqFiche.first();
    affFiche();
    connect(this, SIGNAL(ficheChangee(QString)), ArgumentModel::leModele, SLOT(setNumeroFiche(QString)));
    emit(ficheChangee("1"));

}
/**
 * @brief MainWindow::affFiche
 * affichage d'une fiche
 */
void MainWindow::affFiche()
{
    qDebug()<<"void MainWindow::affFiche()";
    QString titreSupFiche=reqFiche.value("titreSupFiche").toString();
    QString titreInfFiche=reqFiche.value("titreInfFiche").toString();
    QString couleurFiche=reqFiche.value("couleurFiche").toString();

    //affichage
    ui->labelTitreSup->setText(titreSupFiche);
    ui->labelTitreInf->setText(titreInfFiche);
    int numeroFiche=reqFiche.value("numeroFiche").toInt();
    //modif ficheCourante
    numeroFicheCourante=numeroFiche;

    ui->pushButtonLeft->setEnabled(numeroFiche>1);
    //desactivation conditionnelle du bouton droit
    QString txtReqCount="select count(*) from Fiche";
    QSqlQuery reqCount(txtReqCount);
    reqCount.first();
    int nbFiches=reqCount.value(0).toInt();
    ui->pushButtonRight->setEnabled(numeroFiche<nbFiches);
    //changer les styles
    ui->labelTitreSup->setStyleSheet("font-weight:bold;color:"+couleurFiche+";");
    ui->labelTitreInf->setStyleSheet("font-weight:bold;color:"+couleurFiche+";");
    ui->line->setStyleSheet("background-color:"+couleurFiche);
    //affichage de la mesure
    affMesure();
    //affichage des chiffres clé
    affChiffresCle();
}
/**
 * @brief MainWindow::affMesure
 * affiche la mesure correspondant à la fiche
 */
void MainWindow::affMesure()
{
    qDebug()<<"void MainWindow::affMesure()";
    //CREATE TABLE `Mesure`(`numeroMesure` INTEGER,`libelleMesures` VARCHAR(254),`numeroFiche` INTEGER NOT NULL, foreign key (`numeroFiche`) references Fiche(`numeroFiche`),primary key(`numeroMesure`));
    QString txtReqMesure="select libelleMesures from Mesure where numeroFiche="+QString::number(numeroFicheCourante);
    QSqlQuery reqMesure(txtReqMesure);
    reqMesure.first();
    QString libelleMesures=reqMesure.value("libelleMesures").toString();
    ui->labelLibelleMesures->setText(libelleMesures);
    affDetailsMesure();
}
/**
 * @brief MainWindow::affDetailsMesure
 * affiche les details d'une mesure
 */
void MainWindow::affDetailsMesure()
{
    qDebug()<<"void MainWindow::affDetailsMesure()";

    QString txtReqDetailMesure="select texteDetailMesure from DetailMesure where numeroMesure="+QString::number(numeroFicheCourante);
    QSqlQuery reqDetailsMesure(txtReqDetailMesure);
    QString texteDetails="<ul>";
    while(reqDetailsMesure.next())
    {
        texteDetails+="<li>"+reqDetailsMesure.value("texteDetailMesure").toString()+"</li>";
    }
    texteDetails+="</ul>";
    ui->textBrowserDetailsMesure->setHtml(texteDetails);
}

void MainWindow::affChiffresCle()
{
    qDebug()<<"void MainWindow::affChiffresCle()";
    QString texteChiffreCle;
    QString txtReqChiffresCle="select chiffre,unite,texteChiffre from ChiffreCle where numeroFiche="+QString::number(numeroFicheCourante);
    QSqlQuery reqChiffresCle(txtReqChiffresCle);

    while(reqChiffresCle.next())
    {
        texteChiffreCle+="<h1>"+reqChiffresCle.value("chiffre").toString()+" "+reqChiffresCle.value("unite").toString()+"</h1>";
        texteChiffreCle+="<p>"+reqChiffresCle.value("texteChiffre").toString()+"</p>";
    }
    ui->textBrowserChiffresCle->setHtml(texteChiffreCle);
}

/**
 * @brief MainWindow::on_pushButtonLeft_clicked
 * clic sur le bouton arrière
 */
void MainWindow::on_pushButtonLeft_clicked()
{
    qDebug()<<"void MainWindow::on_pushButtonLeft_clicked()";
    numeroFicheCourante--;
    reqFiche.first();
    for(int num=0;num<numeroFicheCourante-1;num++)
    {
        reqFiche.next();
    }
    affFiche();
    emit(ficheChangee(QString::number(this->numeroFicheCourante)));
}
/**
 * @brief MainWindow::on_pushButtonRight_clicked
 * clic sur le bouton avant
 */
void MainWindow::on_pushButtonRight_clicked()
{
    qDebug()<<"void MainWindow::on_pushButtonRight_clicked()";
    if(reqFiche.next())
    {
        affFiche();
        emit(ficheChangee(QString::number(this->numeroFicheCourante)));
    }
}
