import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Controls.Material 2.3
import QtQuick.Controls.Styles 1.4
import Qt.labs.settings 1.1
import QtQuick 2.6
import QtQuick.Controls 2.1
import tomaragorn.nowhere.fr 1.0

Pane {
    id: pane
    SwipeView {
        id: view
        currentIndex: 1
        anchors.fill: parent
        Repeater {
            model: ArgumentModel{numeroFiche:"1"}
            Pane {
                width: view.width
                height: view.height

                Column {
                    spacing: 40
                    width: parent.width

                    Label {
                        width: parent.width
                        wrapMode: Label.Wrap
                        horizontalAlignment: Qt.AlignHCenter
                        text: model.texteArgument
                    }

                    /*Image {
                        source: "../images/arrows.png"
                        anchors.horizontalCenter: parent.horizontalCenter
                    }*/
                }
            }
        }
    }

    PageIndicator {
        count: view.count
        currentIndex: view.currentIndex
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}

