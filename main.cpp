#include "mainwindow.h"
#include <QSqlDatabase>
#include <QDebug>
#include <QApplication>
#include <QtQml>
#include <QStandardPaths>
#include <QSqlError>
#include "argumentmodel.h"

static bool connectToDatabase()
{
    QSqlDatabase database = QSqlDatabase::database();
    if (!database.isValid())
    {
        database = QSqlDatabase::addDatabase("QSQLITE");
        if (!database.isValid())
            qFatal("Cannot add database: %s", qPrintable(database.lastError().text()));
    }
    const QDir writeDir = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    if (!writeDir.mkpath("."))
        qFatal("Failed to create writable directory at %s", qPrintable(writeDir.absolutePath()));

    // Ensure that we have a writable location on all devices.
    const QString fileName = writeDir.absolutePath() + "/baseFicheArguV0.1.sqlite";
    database.setDatabaseName(fileName);
    if (!database.open()) {
        QFile::remove(fileName);
        //qFatal("Cannot open database: %s", qPrintable(database.lastError().text()));
        return false;
    }
    else
        return true;
}
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QSqlDatabase maBase = QSqlDatabase::addDatabase("QSQLITE");
    maBase.setHostName("localhost");
    maBase.setDatabaseName("baseFicheArguV0.1.sqlite");
    if(connectToDatabase())
    {
        qDebug()<<"Base trouvée";
        if (!maBase.contains(QStringLiteral("Fiche")))
        {
               //création de la base
            QFile ficCreationBase(":/new/sql/creBase.sql");
            ficCreationBase.open(QFile::ReadOnly);
            QTextStream texteStream(&ficCreationBase);
            QString texteRequete;
            while(texteStream.readLineInto(&texteRequete))
            {
                QSqlQuery req(texteRequete);
            }
            //insertion du jeu d'essais
            QFile ficJeuEssai(":/new/sql/insereDataOK.sql");
            ficJeuEssai.open(QFile::ReadOnly);
            QTextStream texteStream2(&ficJeuEssai);
            while(texteStream2.readLineInto(&texteRequete))
            {
                QSqlQuery req(texteRequete);
            }
        }
        qmlRegisterType<ArgumentModel>("tomaragorn.nowhere.fr", 1, 0, "ArgumentModel");
        MainWindow w;
        w.showMaximized();
        return a.exec();
    }
    else
    {
        qDebug()<<"Désolé, la base n'a pas pu être créée";
        return(22);
    }
}
