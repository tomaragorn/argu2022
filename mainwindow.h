#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSqlQuery>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
private slots:
    void on_pushButtonLeft_clicked();
    void on_pushButtonRight_clicked();
private:
    Ui::MainWindow *ui;
    void obtenirFiche();
    QSqlQuery reqFiche;
    void affFiche();
    void affMesure();
    void affDetailsMesure();
    void affChiffresCle();
    int numeroFicheCourante;
signals:
    void ficheChangee(QString);
};
#endif // MAINWINDOW_H
